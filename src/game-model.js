const GENERATE = 'Generate';
const BUY_GENERATOR = 'Buy Generator';
const BUY_UPGRADE = 'Buy Upgrade';

class Model {
  constructor(initialTime) {
    const initialState = {
      initialTime,
      checkpointTime: initialTime,
      resources: 0,
      generators: [
        {
          id: 0,
          name: 'Level 1',
          count: 0,
          resourcesPerSecond: 0.1,
          cost: 15
        },
        {
          id: 1,
          name: 'Level 2',
          count: 0,
          resourcesPerSecond: 1,
          cost: 100
        },
        {
          id: 2,
          name: 'Level 3',
          count: 0,
          resourcesPerSecond: 8,
          cost: 1100
        },
        {
          id: 3,
          name: 'Level 4',
          count: 0,
          resourcesPerSecond: 47,
          cost: 12000
        },
        {
          id: 4,
          name: 'Level 5',
          count: 0,
          resourcesPerSecond: 260,
          cost: 130000
        },
        {
          id: 5,
          name: 'Level 6',
          count: 0,
          resourcesPerSecond: 1400,
          cost: 1400000
        },
        {
          id: 6,
          name: 'Level 7',
          count: 0,
          resourcesPerSecond: 7800,
          cost: 20000000
        },
        {
          id: 7,
          name: 'Level 8',
          count: 0,
          resourcesPerSecond: 44000,
          cost: 330000000
        },
        {
          id: 8,
          name: 'Level 9',
          count: 0,
          resourcesPerSecond: 260000,
          cost: 5100000000
        },
        {
          id: 9,
          name: 'Level 10',
          count: 0,
          resourcesPerSecond: 1600000,
          cost: 75000000000
        },
        {
          id: 10,
          name: 'Level 11',
          count: 0,
          resourcesPerSecond: 10000000,
          cost: 1000000000000
        },
        {
          id: 11,
          name: 'Level 12',
          count: 0,
          resourcesPerSecond: 430000000,
          cost: 170000000000000
        },
        {
          id: 12,
          name: 'Level 13',
          count: 0,
          resourcesPerSecond: 2900000000,
          cost: 2100000000000000
        },
        {
          id: 13,
          name: 'Level 14',
          count: 0,
          resourcesPerSecond: 21000000000,
          cost: 26000000000000000
        }
      ]
    };

    this.store = window.Redux.createStore(
      this.reducer.bind(this),
      initialState,
      window.__REDUX_DEVTOOLS_EXTENSION__ &&
        window.__REDUX_DEVTOOLS_EXTENSION__()
    );
  }

  reducer(state, action) {
    switch (action.type) {
      case GENERATE:
        // Manually generate.
        return Object.assign({}, state, { resources: state.resources + 1 });
      case BUY_GENERATOR:
        // Checkpoint the number of resources to this point minus the cost of the new generator.
        let resources =
          this.getResources(action.currentTime) -
          this.generator(action.level).cost;

        // Add the generator and increase the cost of the next generator.
        return Object.assign({}, state, {
          checkpointTime: action.currentTime,
          resources,
          generators: state.generators.map(generator => {
            if (action.level == generator.id) {
              return Object.assign({}, generator, {
                count: generator.count + 1,
                cost: generator.cost * 1.15
              });
            } else {
              return generator;
            }
          })
        });
      case BUY_UPGRADE:
      // Add the upgrade and debit the cost of the upgrade.

      default:
        return state;
    }
  }

  generator(level) {
    return this.store.getState().generators[level];
  }

  /**
   *
   * @param {*} currentTime Included so we can use it for calculations of the
   * current number of resources once we've got generators which generate X
   * number of resources per second.
   */
  getResources(currentTime = Date.now()) {
    let state = this.store.getState();

    // Get the count of resources (this is both manually generated resources and the count
    // as of the last purchase/sale of a generator or update).
    let resourceTotal = state.resources;

    // Calculate the elapsed seconds since the last checkpoint.
    let elapsedMs = currentTime - state.checkpointTime;
    let elapsedSeconds = elapsedMs / 1000;

    // Multiply the resources per second times the number of seconds and add to the total.
    return resourceTotal + elapsedSeconds * this.getResourcesPerSecond(state);
  }

  getResourcesPerSecond(state = this.store.getState()) {
    // Add up all the resources per second we have from all the generators owned.
    let resourcesPerSecond = state.generators.reduce(
      (accumulator, generator) => {
        return accumulator + generator.count * generator.resourcesPerSecond;
      },
      0
    );

    return resourcesPerSecond;
  }

  generate(currentTime = Date.now()) {
    this.store.dispatch({ type: GENERATE, currentTime });
  }

  canBuyGenerator(resources, level) {
    return resources >= this.generator(level).cost;
  }

  buyGenerator(level, currentTime = Date.now()) {
    if (this.canBuyGenerator(this.getResources(currentTime), level)) {
      this.store.dispatch({ type: BUY_GENERATOR, level, currentTime });
    }
  }
}
